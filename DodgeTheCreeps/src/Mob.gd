extends RigidBody2D


onready var animate_sprite: AnimatedSprite = $AnimatedSprite
onready var visibility_notifier: VisibilityNotifier2D = $VisibilityNotifier2D

export var min_speed: = 150
export var max_speed: = 250

var _rng: = RandomNumberGenerator.new()
var _mob_types: = ["walk", "swim", "fly"]


func _ready():
	visibility_notifier.connect("screen_exited", self, "queue_free")
	
	_rng.randomize()
	var idx: = _rng.randi() % _mob_types.size()
	animate_sprite.animation = _mob_types[idx]
