# Godot Poznan 2019 Presentation

GDScript coding guidelines @ GDQuest presentation I gave @ GodotCon 2019 in Poznan - https://godotengine.org/article/schedule-godotcon-2019-poznan

The actual presentation is the `pdf` file.


## Godot demo

I picked `DodgeTheCreeps` Godot demo because it was a simple game that would allow me to parse it fast and to the point. The demo is found in the folder with the same name. The `master` branch holds the "refactored" demo while the `original` branch has the unaltered Godot "official" demo.
